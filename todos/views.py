from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def show_todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list_list}
    return render(request, "todos/list.html", context)


def show_todo_list(request, pk):
    todo_list_instance = TodoList.objects.get(pk=pk)
    context = {"todo_list_instance": todo_list_instance}
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", pk=todo_list.pk)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def update_todo_list(request, pk):
    todo_list_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=pk)
    else:
        form = TodoListForm(instance=todo_list_instance)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todo_list(request, pk):
    todo_list_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        todo_list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", pk=todo_item.list.pk)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/create_item.html", context)


def update_todo_item(request, pk):
    todo_item_instance = TodoItem.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", pk=todo_item_instance.list.pk)
    else:
        form = TodoItemForm(instance=todo_item_instance)

    context = {"form": form}

    return render(request, "todos/edit_item.html", context)
